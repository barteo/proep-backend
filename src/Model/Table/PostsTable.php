<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Posts Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\TypesTable|\Cake\ORM\Association\BelongsTo $Types
 *
 * @method \App\Model\Entity\Post get($primaryKey, $options = [])
 * @method \App\Model\Entity\Post newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Post[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Post|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Post patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Post[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Post findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PostsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('posts');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Types', [
            'foreignKey' => 'type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->notEmpty('title');

        $validator
            ->scalar('address')
            ->maxLength('address', 255)
            ->notEmpty('address');

        $validator
            ->scalar('postcode')
            ->maxLength('postcode', 255)
            ->notEmpty('postcode');

        $validator
            ->scalar('city')
            ->maxLength('city', 255)
            ->notEmpty('city');

        $validator
            ->boolean('incl')
            ->notEmpty('incl');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->notEmpty('description');

        $validator
            ->integer('price')
            ->notEmpty('price');

        $validator
            ->integer('bedrooms')
            ->notEmpty('bedrooms');

        $validator
            ->boolean('furnished')
            ->notEmpty('furnished');

        $validator
            ->scalar('latitude')
            ->maxLength('latitude', 255)
            ->notEmpty('latitude');

        $validator
            ->scalar('longtitude')
            ->maxLength('longtitude', 255)
            ->notEmpty('longtitude');

        $validator
            ->boolean('active')
            ->notEmpty('active');

        $validator
            ->integer('minimum')
            ->notEmpty('minimum');

        $validator
            ->scalar('image')
            ->maxLength('image', 255)
            ->notEmpty('image');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['type_id'], 'Types'));

        return $rules;
    }
}
