<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Post Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $type_id
 * @property string $title
 * @property string $address
 * @property string $postcode
 * @property string $city
 * @property bool $incl
 * @property string $description
 * @property int $price
 * @property int $bedrooms
 * @property bool $furnished
 * @property string $latitude
 * @property string $longtitude
 * @property bool $active
 * @property int $minimum
 * @property string $image
 * @property \Cake\I18n\FrozenDate $created
 * @property \Cake\I18n\FrozenDate $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Type $type
 */
class Post extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'type_id' => true,
        'title' => true,
        'address' => true,
        'postcode' => true,
        'city' => true,
        'incl' => true,
        'description' => true,
        'price' => true,
        'bedrooms' => true,
        'furnished' => true,
        'latitude' => true,
        'longtitude' => true,
        'active' => true,
        'minimum' => true,
        'image' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'type' => true
    ];
}
