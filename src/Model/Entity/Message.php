<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Message Entity
 *
 * @property int $id
 * @property int $conversation_id
 * @property int $user_id
 * @property string $message
 * @property string $ip
 * @property \Cake\I18n\FrozenDate $created
 * @property \Cake\I18n\FrozenDate $modified
 *
 * @property \App\Model\Entity\Conversation $conversation
 * @property \App\Model\Entity\User $user
 */
class Message extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'conversation_id' => true,
        'user_id' => true,
        'message' => true,
        'ip' => true,
        'created' => true,
        'modified' => true,
        'conversation' => true,
        'user' => true
    ];
}
