<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use App\Model\Entity\Meeting;

/**
 * Meetings Controller
 *
 * @property \App\Model\Table\MeetingsTable $Meetings
 *
 * @method \App\Model\Entity\Meeting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MeetingsController extends AppController
{
    public function getPendingMeetings()
    {
      $this->set(['success' => true]);

      $pendingMeetings = $this->Meetings->find('all')
        ->where(['owner_id' => $this->Auth->user('id')])
        ->andWhere(['owner_accepted' => 0])
        ->contain(['Users']);

      $this->set('data', [
        'pendingMeetings' => $pendingMeetings,
      ]);

      $this->set('_serialize', ['success', 'data']);
    }

    public function getAcceptedMeetings()
    {
      $this->set(['success' => true]);

      $acceptedMeetings = $this->Meetings->find('all')
        ->where(['owner_id' => $this->Auth->user('id')])
        ->orWhere(['viewer_id' => $this->Auth->user('id')])
        ->andWhere(['owner_accepted' => 1])
        ->contain(['Users']);

      $this->set('data', [
        'acceptedMeetings' => $acceptedMeetings,
      ]);

      $this->set('_serialize', ['success', 'data']);
    }

    public function acceptMeeting()
    {
      $meeting = $this->Meetings->get($this->request->getData('meeting_id'));

      if($meeting->owner_id == $this->Auth->user('id')){
        $meeting->owner_accepted = 1;
        if ($this->Meetings->save($meeting)){
          $this->set(['success' => true]);
          $this->set(['data' => 'You have accepted a meeting.']);
        } else {
          $this->set(['success' => false]);
          $this->set(['data' => 'Error accepting a meeting.']);
        }
      }

      $this->set('_serialize', ['success', 'data']);
    }

    public function declineMeeting()
    {
      $meeting = $this->Meetings->get($this->request->getData('meeting_id'));

      if($meeting->owner_id == $this->Auth->user('id')){
        $meeting->owner_accepted = -1;
        if ($this->Meetings->save($meeting)){
          $this->set(['success' => true]);
          $this->set(['data' => 'You have declined a meeting.']);
        } else {
          $this->set(['success' => false]);
          $this->set(['data' => 'Error declining a meeting.']);
        }
      }

      $this->set('_serialize', ['success', 'data']);
    }

    public function scheduleViewing()
    {
      $this->loadModel('Posts');

      $post = $this->Posts->get($this->request->getData('post_id'));

      if ($post->user_id != $this->Auth->user('id')) {
        $meeting = new Meeting();

        $meeting->owner_id = $post->user_id;
        $meeting->viewer_id = $this->Auth->user('id');
        $meeting->post_id = $post->id;
        $meeting->meeting_time = $this->request->getData('meeting_time');

        if($this->Meetings->save($meeting)){
          $this->set(['success' => true]);
          $this->set(['data' => 'You have requested a meeting.']);
        } else {
          $this->set(['success' => false]);
          $this->set(['data' => 'Error requesting a meeting.']);
        }
      } else {
        $this->set(['success' => false]);
        $this->set(['data' => 'Error requesting a meeting.']);
      }

      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $meeting = $this->Meetings->newEntity();
        if ($this->request->is('post')) {
            $meeting = $this->Meetings->patchEntity($meeting, $this->request->getData());
          $meeting->user_id = $this->Auth->user('id');
            if ($this->Meetings->save($meeting)) {
              $this->set(['success' => true]);
              $this->set(['data' => 'You have created a new meeting.']);
            } else {
              $this->set(['success' => false]);
              $this->set(['data' => 'Error creating a new meeting.']);
            }
        }
      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Meeting id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set(['success' => true]);
        $meeting = $this->Meetings->get($this->request->getData('id'), [
            'contain' => []
        ]);
        if ($this->request->is('patch', 'post', 'put')) {
           $meeting = $this->Meetings->patchEntity($meeting, $this->request->getData());
          $meeting->user_id = $this->Auth->user('id');
            if ($this->Meetings->save($meeting)) {
              $this->set(['success' => true]);
              $this->set(['data' => 'Edit Success.']);
            } else {
              $this->set(['success' => false]);
              $this->set(['data' => 'Error.']);
            }
        }
      $this->set('_serialize', ['success', 'data']);
    }
}
