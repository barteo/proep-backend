<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
/**
 * Conversations Controller
 *
 * @property \App\Model\Table\ConversationsTable $Conversations
 *
 * @method \App\Model\Entity\Conversation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ConversationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->set(['success' => true]);
        $this->paginate = [
            'contain' => ['Users']
        ];
        $conversations = $this->paginate($this->Conversations);

      $this->set('data', [
        'conversations' => $conversations,
      ]);

      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * View method
     *
     * @param string|null $id Conversation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set(['success' => true]);
        $conversation = $this->Conversations->get($this->request->getData('id'), [
            'contain' => ['Users', 'Messages']
        ]);

      $this->set('data', [
        'conversation' => $conversation,
      ]);

      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
       $conversation = $this->Conversations->newEntity();
        if ($this->request->is('post')) {
            $conversation = $this->Conversations->patchEntity($conversation, $this->request->getData());
          $conversation->user_id = $this->Auth->user('id');
            if ($this->Conversations->save($conversation)) {
              $this->set(['success' => true]);
              $this->set(['data' => 'You have created a new conversation.']);
            } else {
              $this->set(['success' => false]);
              $this->set(['data' => 'Error creating a new conversation.']);
            }
        }
      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Conversation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set(['success' => true]);
        $conversation = $this->Conversations->get($this->request->getData('id'), [
            'contain' => []
        ]);
        if ($this->request->is('patch', 'post', 'put')) {
           $conversation = $this->Conversations->patchEntity($conversation, $this->request->getData());
          $conversation->user_id = $this->Auth->user('id');
            if ($this->Conversations->save($conversation)) {
              $this->set(['success' => true]);
              $this->set(['data' => 'Edit Success.']);
            } else {
              $this->set(['success' => false]);
              $this->set(['data' => 'Error.']);
            }
        }
      $this->set('_serialize', ['success', 'data']);

    }

    /**
     * Delete method
     *
     * @param string|null $id Conversation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $conversation = $this->Conversations->get($this->request->getData('id');
        if ($this->Conversations->delete($conversation)){
            $this->set(['success' => true]);
              $this->set(['data' => 'Successfully Deleted.']);
            } else {
              $this->set(['success' => false]);
              $this->set(['data' => 'Error.']);
        }
    }
}
