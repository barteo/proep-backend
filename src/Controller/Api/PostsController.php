<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 *
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{

  //Initialize function allowing add and token action without authorization
  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow(['index']);
  }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
      $this->set(['success' => true]);
        $this->paginate = [
            'limit' => 6,
            'contain' => ['Users', 'Types']
        ];
        $posts = $this->paginate($this->Posts);

      $this->set('data', [
        'posts' => $posts,
      ]);

      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * View method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view()
    {
      $this->set(['success' => true]);
      $post = $this->Posts->get($this->request->getData('post_id'), [
          'contain' => ['Users', 'Types']
      ]);

      $this->set('data', [
        'post' => $post,
      ]);

      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $post = $this->Posts->newEntity();
        if ($this->request->is('post')) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
          $post->user_id = $this->Auth->user('id');
            if ($this->Posts->save($post)) {
              $this->set(['success' => true]);
              $this->set(['data' => 'You have created a new post.']);
            } else {
              $this->set(['success' => false]);
              $this->set(['data' => 'Error creating a new post.']);
            }
        }
      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set(['success' => true]);
        $post = $this->Posts->get($this->request->getData('id'), [
            'contain' => []
        ]);
        if ($this->request->is('patch', 'post', 'put')) {
           $post = $this->Posts->patchEntity($post, $this->request->getData());
          $post->user_id = $this->Auth->user('id');
            if ($this->Posts->save($post)) {
              $this->set(['success' => true]);
              $this->set(['data' => 'Edit Success.']);
            } else {
              $this->set(['success' => false]);
              $this->set(['data' => 'Error.']);
            }
        }
      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $post = $this->Posts->get($this->request->getData('id'));
        if ($this->Posts->delete($post)){
            $this->set(['success' => true]);
              $this->set(['data' => 'Successfully Deleted.']);
            } else {
              $this->set(['success' => false]);
              $this->set(['data' => 'Error.']);
        }
    }

    public function search()
    {
      $this->set(['success' => true]);

      $search_text = $this->request->getData('search_text');

      $posts = $this->Posts->find('all')
        ->where(['title LIKE' => '%'. $search_text .'%'])
        ->orWhere(['address LIKE' => '%'. $search_text .'%'])
        ->orWhere(['postcode LIKE' => '%'. $search_text .'%'])
        ->orWhere(['city LIKE' => '%'. $search_text .'%'])
        ->contain(['Users', 'Types']);

      $this->set('data', [
        'posts' => $posts,
      ]);

      $this->set('_serialize', ['success', 'data']);
    }
}
