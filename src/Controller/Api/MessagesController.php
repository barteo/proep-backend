<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Messages Controller
 *
 * @property \App\Model\Table\MessagesTable $Messages
 *
 * @method \App\Model\Entity\Message[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MessagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->set(['success' => true]);
        $this->paginate = [
            'contain' => ['Conversations','Users']
        ];
        $messages = $this->paginate($this->Messages);

      $this->set('data', [
        'messages' => $messages,
      ]);

      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * View method
     *
     * @param string|null $id Message id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set(['success' => true]);
        $message = $this->Messages->get($this->request->getData('id'), [
            'contain' => ['Conversations','Users']
        ]);

      $this->set('data', [
        'message' => $message,
      ]);

      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $message = $this->Messages->newEntity();
        if ($this->request->is('post')) {
            $message = $this->Messages->patchEntity($message, $this->request->getData());
          $message->user_id = $this->Auth->user('id');
            if ($this->Messages->save($message)) {
              $this->set(['success' => true]);
              $this->set(['data' => 'You have created a new message.']);
            } else {
              $this->set(['success' => false]);
              $this->set(['data' => 'Error creating a new message.']);
            }
        }
      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Message id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set(['success' => true]);
        $message = $this->Messages->get($this->request->getData('id'), [
            'contain' => []
        ]);
        if ($this->request->is('patch', 'post', 'put')) {
           $message = $this->Messages->patchEntity($message, $this->request->getData());
          $message->user_id = $this->Auth->user('id');
            if ($this->Messages->save($message)) {
              $this->set(['success' => true]);
              $this->set(['data' => 'Edit Success.']);
            } else {
              $this->set(['success' => false]);
              $this->set(['data' => 'Error.']);
            }
        }
      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Message id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $message = $this->Messages->get($this->request->getData('id');
        if ($this->Messages->delete($message)){
            $this->set(['success' => true]);
              $this->set(['data' => 'Successfully Deleted.']);
            } else {
              $this->set(['success' => false]);
              $this->set(['data' => 'Error.']);
        }
    }
}
