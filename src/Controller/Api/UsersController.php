<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

  //Initialize function allowing add and token action without authorization
  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow(['add', 'token']);
  }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
      $users = $this->paginate($this->Users);

      $this->set('data', [
        'users' => $users,
      ]);

      $this->set('_serialize', ['data']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
      $this->Crud->on('afterSave', function(Event $event) {
        if ($event->subject->created) {
          $this->set('data', [
            'id' => $event->subject->entity->id,
            'token' => JWT::encode(
              [
                'sub' => $event->subject->entity->id,
                'exp' =>  time() + 604800
              ],
              Security::salt())
          ]);
          $this->Crud->action()->config('serialize.data', 'data');
        }
      });
      return $this->Crud->execute();
    }

    //Get token method
    public function token(){
      $user = $this->Auth->identify();
      $this->log($this->request->data(), 'debug');
      if (!$user) {
        throw new UnauthorizedException('Invalid email or password');
      }

      $this->set([
        'success' => true,
        'data' => [
          'token' => JWT::encode([
            'sub' => $user['id'],
            'exp' =>  time() + 604800
          ],
            Security::salt())
        ],
        '_serialize' => ['success', 'data']
      ]);
    }
}
