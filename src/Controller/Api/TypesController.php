<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Types Controller
 *
 * @property \App\Model\Table\TypesTable $Types
 *
 * @method \App\Model\Entity\Type[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->set(['success' => true]);
        $types = $this->paginate($this->Types);
        $this->set('data', [
          'types' => $types,
        ]);

      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * View method
     *
     * @param string|null $id Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set(['success' => true]);
        $type = $this->Types->get($this->request->getData('id'), [
            'contain' => ['Posts']
        ]);

      $this->set('data', [
        'type' => $type,
      ]);

      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $type = $this->Types->newEntity();
        if ($this->request->is('post')) {
            $type = $this->Types->patchEntity($type, $this->request->getData());
          $type->user_id = $this->Auth->user('id');
            if ($this->Types->save($type)) {
              $this->set(['success' => true]);
              $this->set(['data' => 'You have created a new type.']);
            } else {
              $this->set(['success' => false]);
              $this->set(['data' => 'Error creating a new type.']);
            }
        }
      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set(['success' => true]);
        $type = $this->Types->get($this->request->getData('id'), [
            'contain' => []
        ]);
        if ($this->request->is('patch', 'post', 'put')) {
           $type = $this->Types->patchEntity($type, $this->request->getData());
          $type->user_id = $this->Auth->user('id');
            if ($this->Types->save($type)) {
              $this->set(['success' => true]);
              $this->set(['data' => 'Edit Success.']);
            } else {
              $this->set(['success' => false]);
              $this->set(['data' => 'Error.']);
            }
        }
      $this->set('_serialize', ['success', 'data']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $type = $this->Types->get($this->request->getData('id'));
        if ($this->Types->delete($type)){
            $this->set(['success' => true]);
              $this->set(['data' => 'Successfully Deleted.']);
            } else {
              $this->set(['success' => false]);
              $this->set(['data' => 'Error.']);
        }
    }
}
